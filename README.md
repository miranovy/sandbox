# Sandbox

Nette sandbox with AdminLTE build on Nuttela Project (see https://github.com/planette/nutella-project).

## Controls

### Paginator

```php
use App\Modules\Base\Control\Paginator\TPaginator;

class FooPresenter extends SinglePagePresenter
{
    use TPaginator;

    /**
     * Render default page.
     *
     * @param int $page Selected page number
     */
    public function renderDefault(int $page = 1): void
    {
        ...
        $this->createPaginator($page, $itemCount, $itemsPerPage);
    }
}
```

Add to layout:
```html
 <script type="text/javascript"  src="/js/responsive-paginate.js"></script>
    <script>
        $(document).ready(function () {
            $(".pagination").rPage();
        });
    </script>
```
