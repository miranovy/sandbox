CREATE TABLE `user` (
  `userId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `state` int(11) unsigned NOT NULL DEFAULT '1',
  `password` varchar(255) NOT NULL,
  `role` varchar(25) NOT NULL DEFAULT 'user',
  `lastLoggedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;