.PHONY: qa lint cs csf phpstan tests coverage

all:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$' | xargs

vendor: composer.json composer.lock
	composer install --no-progress

qa: cs lint phpstan

lint: vendor
	vendor/bin/linter app tests

cs: vendor
	vendor/bin/codesniffer app tests

csf: vendor
	vendor/bin/codefixer app tests

phpstan: vendor
	vendor/bin/phpstan analyse -l max -c phpstan.neon app tests

tests: vendor
	vendor/bin/tester -s -p php --colors 1 -C tests

coverage: vendor
	vendor/bin/tester -s -p phpdbg --colors 1 -C --coverage ./coverage.html --coverage-src app tests
