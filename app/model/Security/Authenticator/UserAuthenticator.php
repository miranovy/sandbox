<?php

declare(strict_types=1);

namespace App\Model\Security\Authenticator;

use App\Model\Database\Repository\UserRepository;
use App\Model\Exception\Runtime\AuthenticationException;
use App\Model\Orm\Orm;
use App\Model\Security\Passwords;
use Nette\Security\IAuthenticator;
use Nette\Security\IIdentity;
use Nextras\Dbal\Utils\DateTimeImmutable;

final class UserAuthenticator implements IAuthenticator
{

    /** @var Orm Orm */
    private $orm;

    /** @var Passwords Passwords utility */
    private $passwords;

    /**
     * Class constructor.
     *
     * @param Orm $orm
     * @param Passwords $passwords
     */
    public function __construct(Orm $orm, Passwords $passwords)
    {
        $this->orm = $orm;
        $this->passwords = $passwords;
    }

    /**
     * Authenticate user.
     *
     * @param string[] $credentials Credentials (username, password)
     * @return IIdentity
     * @throws AuthenticationException
     */
    public function authenticate(array $credentials): IIdentity
    {
        [$username, $password] = $credentials;

        $user = $this->orm->users->getByEmail($username);

        if ($user === null) {
            throw new AuthenticationException('The username is incorrect.', self::IDENTITY_NOT_FOUND);
        } elseif (!$user->isActivated()) {
            throw new AuthenticationException('The user is not active.', self::INVALID_CREDENTIAL);
        } elseif (!$this->passwords->verify($password, $user->password)) {
            throw new AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);
        } elseif ($this->passwords->needsRehash($user->password)) {
            $user->password = $this->passwords->hash($password);
        }

        $user->lastLoggedAt = new DateTimeImmutable('now');
        $this->orm->persistAndFlush($user);

        return $user->toIdentity();
    }
}
