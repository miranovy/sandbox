<?php

declare(strict_types=1);

namespace App\Model\Security;

use App\Model\Orm\Users\User;
use Nette\Security\User as NetteUser;

/**
 * @method Identity getIdentity()
 */
final class SecurityUser extends NetteUser
{

    /**
     * Return if is user admin.
     *
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->isInRole(User::ROLE_ADMIN);
    }
}
