<?php declare(strict_types = 1);

namespace App\Model\Router;

use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

/**
 * Router factory class.
 */
final class RouterFactory
{

    /**
     * Create routes.
     *
     * @return RouteList<Route>
     */
    public function create(): RouteList
    {
        $router = new RouteList();

        $this->buildAdmin($router);
        $this->buildFront($router);

        return $router;
    }

    /**
     * Build admin routes.
     *
     * @param RouteList<Route> $router
     * @return RouteList<Route>
     */
    protected function buildAdmin(RouteList $router): RouteList
    {
        $router[] = $list = new RouteList('Admin');
        $list[] = new Route('admin/<presenter>/<action>[/<id>]', 'Homepage:default');

        return $list;
    }

    /**
     * Build front routes.
     *
     * @param RouteList<Route> $router
     * @return RouteList<Route>
     */
    protected function buildFront(RouteList $router): RouteList
    {
        $router[] = $list = new RouteList('Front');
        $list[] = new Route('<presenter>/<action>[/<id>]', 'Home:default');

        return $list;
    }
}
