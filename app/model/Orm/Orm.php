<?php declare(strict_types=1);

namespace App\Model\Orm;

use App\Model\Orm\Users\UsersRepository;
use Nextras\Orm\Model\Model;

/**
 * Orm model.
 *
 * @property-read UsersRepository $users
 */
class Orm extends Model
{

}
