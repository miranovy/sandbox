<?php declare(strict_types=1);

namespace App\Model\Orm\Users;

use Nextras\Orm\Mapper\Dbal\StorageReflection\UnderscoredStorageReflection;
use Nextras\Orm\Mapper\Mapper;

/**
 * Users mapper class.
 */
class UsersMapper extends Mapper
{
    /**
     * Return database table name.
     *
     * @return string
     */
    public function getTableName(): string
    {
        return 'user';
    }

    /**
     * Create storage reflection.
     *
     * @return UnderscoredStorageReflection
     */
    protected function createStorageReflection(): UnderscoredStorageReflection
    {
        $reflection = parent::createStorageReflection();
        $reflection->addMapping('lastLoggedAt', 'lastLoggedAt');

        return $reflection;
    }
}
