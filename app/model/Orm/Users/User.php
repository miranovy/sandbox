<?php declare(strict_types=1);


namespace App\Model\Orm\Users;

use App\Model\Security\Identity;
use Nextras\Dbal\Utils\DateTimeImmutable;
use Nextras\Orm\Entity\Entity;

/**
 * User entity.
 *
 * @property int $id User identifier {primary}
 * @property string $name User name
 * @property string $surname User surname
 * @property string $email Email
 * @property string $password Password hash
 * @property string $role Role {enum self::ROLE_*}
 * @property int $state User state {enum self::STATE_*}
 * @property DateTimeImmutable $lastLoggedAt Last logged at
 * @property-read string $fullName User full name (name + surname) {virtual}
 */
class User extends Entity
{
    public const ROLE_USER = 'user';
    public const ROLE_ADMIN = 'admin';

    public const STATE_FRESH = 1;
    public const STATE_ACTIVATED = 2;
    public const STATE_BLOCKED = 3;

    public const ROLES = [
        self::ROLE_ADMIN => 'user.role_admin',
        self::ROLE_USER => 'user.role_user',
    ];

    public const STATES = [
        self::STATE_FRESH => 'user.state_fresh',
        self::STATE_ACTIVATED => 'user.state_activated',
        self::STATE_BLOCKED => 'user.state_blocked',
    ];

    /**
     * Return user activated flag.
     *
     * @return boolean
     */
    public function isActivated(): bool
    {
        return $this->state === self::STATE_ACTIVATED;
    }

    /**
     * Return gravatar url.
     *
     * @return string
     */
    public function gravatar(): string
    {
        return sprintf('https://www.gravatar.com/avatar/%s', md5($this->email));
    }

    /**
     * Return identity of user.
     *
     * @return Identity
     */
    public function toIdentity(): Identity
    {
        return new Identity($this->id, [$this->role], [
            'email' => $this->email,
            'name' => $this->name,
            'surname' => $this->surname,
            'state' => $this->state,
            'gravatar' => $this->gravatar(),
        ]);
    }

    /**
     * Return user full name.
     *
     * @return string
     */
    protected function getterFullName(): string
    {
        return sprintf('%s %s', $this->name, $this->surname);
    }
}
