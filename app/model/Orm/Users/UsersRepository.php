<?php declare(strict_types=1);

namespace App\Model\Orm\Users;

use Nextras\Orm\Repository\Repository;

/**
 * Users repository class.
 */
class UsersRepository extends Repository
{
    public static function getEntityClassNames(): array
    {
        return [User::class];
    }

    /**
     * Get user by email.
     *
     * @param string $email
     * @return User|null
     */
    public function getByEmail(string $email): ?User
    {
        /** @var User $user */
        $user = $this->getBy(['email' => $email]);

        return $user;
    }
}
