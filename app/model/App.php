<?php declare(strict_types = 1);

namespace App\Model;

final class App
{

    public const DESTINATION_FRONT_HOMEPAGE = ':Front:Home:';
    public const DESTINATION_ADMIN_HOMEPAGE = ':Admin:Homepage:';
}
