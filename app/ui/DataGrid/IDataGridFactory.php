<?php declare(strict_types = 1);

namespace App\UI\DataGrid;

use Nette\ComponentModel\IContainer;
use Nette\Localication\ITranslator;

interface IDataGridFactory
{

    /**
     * Create data grid.
     *
     * @param IContainer|null $parent Parent container
     * @param string|null $name Grid name
     */
    public function create(?IContainer $parent = null, ?string $name = null): DataGrid;
}
