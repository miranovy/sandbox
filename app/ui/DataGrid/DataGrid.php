<?php

declare(strict_types=1);

namespace App\UI\DataGrid;

use Ublaboo\DataGrid\DataGrid as ContributteDataGrid;
use Nette\ComponentModel\IContainer;
use Nette\Localization\ITranslator;

final class DataGrid extends ContributteDataGrid
{

    /**
     * Class constructor.
     *
     * @param IContainer|null $parent Parent container
     * @param string|null $name Grid name
     * @param ITranslator|null $translator Localization translator
     */
    public function __construct(?IContainer $parent = null, ?string $name = null, ?ITranslator $translator = null)
    {
        parent::__construct($parent, $name);

        if ($translator !== null) {
            $this->setTranslator($translator);
        }

        // customization
        $this->setCustomPaginatortemplate(__DIR__ . '/paginator.latte');
        $this->setStrictSessionFilterValues(false);
        $this->setColumnsHideable();
    }

    /**
     * Translate column replacement.
     *
     * @param string[] $replacements Replacements
     * @return string[]
     */
    public function translateColumnReplacement(array $replacements): array
    {
        if ($this->translator === null) {
            return $replacements;
        }

        $translates = [];
        foreach ($replacements as $key => $replacement) {
            $translates[$key] = $this->translator->translate($replacement);
        }
        return $translates;
    }
}
