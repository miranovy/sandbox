<?php


namespace App\UI\Form;

use Nette\Utils\ArrayHash;
use Nextras\Orm\Entity\Entity;

/**
 * Orm form factory interface.
 */
interface IOrmFormFactory
{
    /**
     * Return entity.
     *
     * @param int $id
     * @return Entity|null
     */
    public function getEntity(int $id): ?Entity;

    /**
     * Create entity.
     *
     * @return Entity
     */
    public function createEntity(): Entity;

    /**
     * Update entity.
     *
     * @param Entity $album
     * @param ArrayHash $values
     * @return void
     */
    public function updateEntity(Entity $album, ArrayHash $values): void;
}
