<?php


namespace App\UI\Form;

use Nette\Utils\ArrayHash;

/**
 * Form factory interface.
 */
interface IFormFactory
{
    /**
     * Form succeed action.
     *
     * @param BaseForm $form Form
     * @param ArrayHash<string> $values Form values
     * @return void
     */
    public function processForm(BaseForm $form, ArrayHash $values): void;

    /**
     * Add items to form.
     *
     * @param BaseForm $form
     * @return BaseForm
     */
    public function addItems(BaseForm $form): BaseForm;

    /**
     * Return empty record default values.
     *
     * @return array<string, mixed>
     */
    public function getDefaultValues(): array;

    /**
     * Return record values.
     *
     * @param int $recordId
     * @return array<string, mixed>
     */
    public function getRecordValues(int $recordId): array;
}
