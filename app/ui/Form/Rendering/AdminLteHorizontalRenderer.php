<?php

declare(strict_types=1);

namespace App\UI\Form\Rendering;

use Contributte\Forms\Rendering\Bootstrap4HorizontalRenderer;

/**
 * Admin LTE v3 horizontal renderer.
 */
class AdminLteHorizontalRenderer extends Bootstrap4HorizontalRenderer
{

    /** @var mixed[] */
    public $wrappers = [
        'form' => [
            'container' => null,
        ],
        'error' => [
            'container' => 'div class="alert alert-danger"',
            'item' => 'p',
        ],
        'group' => [
            'container' => 'fieldset',
            'label' => 'legend',
            'description' => 'p',
        ],
        'controls' => [
            'container' => 'div',
        ],
        'pair' => [
            'container' => 'div class="form-group row"',
            '.required' => 'required',
            '.optional' => null,
            '.odd' => null,
        ],
        'control' => [
            'container' => 'div class="col col-sm-9"',
            '.odd' => null,
            'description' => 'span class="form-text"',
            'requiredsuffix' => '',
            'errorcontainer' => 'span class="form-text text-danger"',
            'erroritem' => '',
            '.required' => 'required',
            '.text' => 'text',
            '.password' => 'text',
            '.file' => 'text',
            '.submit' => 'button',
            '.image' => 'imagebutton',
            '.button' => 'button',
            '.error' => 'is-invalid',
        ],
        'label' => [
            'container' => '',
            'suffix' => null,
            'requiredsuffix' => '',
        ],
        'hidden' => [
            'container' => 'div',
        ],
    ];
}
