<?php

declare(strict_types=1);

namespace App\UI\Form\Rendering;

use Contributte\Forms\Rendering\Bootstrap4VerticalRenderer;

/**
 * Admin LTE v3 vertical renderer.
 */
class AdminLteVerticalRenderer extends Bootstrap4VerticalRenderer
{

    /** @var mixed[] */
    public $wrappers = [
        'form' => [
            'container' => null,
        ],
        'error' => [
            'container' => 'div class="alert alert-danger"',
            'item' => 'p',
        ],
        'group' => [
            'container' => 'fieldset',
            'label' => 'legend',
            'description' => 'p',
        ],
        'controls' => [
            'container' => null,
        ],
        'pair' => [
            'container' => 'div class="form-group"',
            '.required' => 'required',
            '.optional' => null,
            '.odd' => null,
        ],
        'control' => [
            'container' => '',
            '.odd' => null,
            'description' => 'span class="form-text"',
            'requiredsuffix' => '',
            'errorcontainer' => 'span class="form-text text-danger"',
            'erroritem' => '',
            '.required' => 'required',
            '.text' => 'text',
            '.password' => 'text',
            '.file' => 'text',
            '.submit' => 'button',
            '.image' => 'imagebutton',
            '.button' => 'button',
            '.error' => 'is-invalid',
        ],
        'label' => [
            'container' => '',
            'suffix' => null,
            'requiredsuffix' => '',
        ],
        'hidden' => [
            'container' => 'div',
        ],
    ];
}
