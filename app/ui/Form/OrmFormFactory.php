<?php declare(strict_types=1);


namespace App\UI\Form;

use App\Model\Orm\Orm;
use Nette\Localization\ITranslator;
use Nette\Utils\ArrayHash;
use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Entity\ToArrayConverter;

/**
 * Orm form factory.
 */
abstract class OrmFormFactory extends FormFactory implements IOrmFormFactory
{
    /** @var Entity|null Edited entity */
    private $entity;

    /** @var Orm */
    protected $orm;

    /**
     * Class constructor.
     *
     * @param ITranslator $translator
     * @param Orm $orm
     */
    public function __construct(ITranslator $translator, Orm $orm)
    {
        parent::__construct($translator);
        $this->orm = $orm;
    }

    /**
     * Create form.
     *
     * @param int|null $id Album identifier
     * @param callable|null $onSuccess Call on success login
     * @return BaseForm
     */
    public function create(?int $id = null, ?callable $onSuccess = null): BaseForm
    {
        // select edited entity
        if ($id !== null) {
            $this->entity = $this->getEntity($id);
            if ($this->entity === null) {
                throw new \RuntimeException('Edited entity not found!');
            }
        }

        $form = parent::create($id, $onSuccess);
        $form->addProtection('Vypršel časový limit, odešlete formulář znovu');

        return $form;
    }

    /**
     * Form succeed action.
     *
     * @param BaseForm $form Form
     * @param ArrayHash<string> $values Form values
     * @return void
     */
    public function processForm(BaseForm $form, ArrayHash $values): void
    {
        $entity = $this->entity;
        if ($entity === null) {
            $entity = $this->createEntity();
        }

        $this->updateEntity($entity, $values);
        $this->orm->persistAndFlush($entity);
    }

    /**
     * @param int $recordId
     * @return array<string, mixed>
     */
    public function getRecordValues(int $recordId): array
    {
        /** @var Entity $entity */
        $entity = $this->entity;

        return $entity->toArray(ToArrayConverter::RELATIONSHIP_AS_ID);
    }
}
