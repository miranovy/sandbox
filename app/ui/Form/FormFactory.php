<?php declare(strict_types=1);

namespace App\UI\Form;

use Nette\Localization\ITranslator;
use Nette\Utils\ArrayHash;

/**
 * Form factory.
 */
abstract class FormFactory implements IFormFactory
{
    /** @var ITranslator Translator */
    private $translator;

    /**
     * Class constructor.
     *
     * @param ITranslator $translator Translator
     */
    public function __construct(ITranslator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Create form.
     *
     * @param int|null $recordId Album identifier
     * @param callable|null $onSuccess Call on success login
     * @return BaseForm
     */
    public function create(?int $recordId = null, ?callable $onSuccess = null): BaseForm
    {
        $form = $this->addItems(new BaseForm());
        $form->setTranslator($this->translator);
        $form->setRecordId($recordId);
        $form->setDefaults($recordId === null ? $this->getDefaultValues() : $this->getRecordValues($recordId));

        // process form
        $form->onSuccess[] = [$this, 'processForm'];
        if ($onSuccess !== null) {
            $form->onSuccess[] = $onSuccess;
        }

        return $form;
    }
}
