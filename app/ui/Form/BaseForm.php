<?php declare(strict_types=1);

namespace App\UI\Form;

use Nette\Application\UI\Form;
use Nette\Forms\Controls\TextInput;
use Nette\Forms\Controls\TextArea;

/**
 * Base form.
 */
class BaseForm extends Form
{
    /** @var int|null Record identifier */
    private $recordId;

    /**
     * Add float number input.
     *
     * @param string $name
     * @param string|null $label
     * @return TextInput
     */
    public function addFloat(string $name, ?string $label = null): TextInput
    {
        $input = self::addText($name, $label);
        $input->addCondition(self::FILLED)
            ->addRule(self::MAX_LENGTH, null, 255)
            ->addRule(self::FLOAT);

        return $input;
    }

    /**
     * Add numeric input.
     *
     * @param string $name
     * @param string|null $label
     * @return TextInput
     */
    public function addNumeric(string $name, ?string $label = null): TextInput
    {
        $input = self::addText($name, $label);
        $input->addCondition(self::FILLED)
            ->addRule(self::MAX_LENGTH, 'Maximální povolený počet znaků je %d', 25)
            ->addRule(self::NUMERIC, 'Pole musí obsahovat pouze číslice');

        return $input;
    }

    /**
     * Add tinymce editor text area.
     *
     * @param string $name
     * @param string $label
     * @param string $class
     * @param int|null $cols
     * @param int|null $rows
     * @return TextArea
     */
    public function addTinymceEditor(string $name, ?string $label = null, ?int $cols = null, ?int $rows = null, string $class = 'tinymce'): TextArea
    {
        $input = self::addTextArea($name, $label, $cols, $rows);
        $input->setHtmlAttribute('class', $class);

        return $input;
    }

    /**
     * Set record identifier.
     *
     * @param integer|null $recordId Record identifier
     * @return void
     */
    public function setRecordId(?int $recordId): void
    {
        $this->recordId = $recordId;
    }

    /**
     * Return record identifier
     *
     * @return integer|null
     */
    public function getRecordId(): ?int
    {
        return $this->recordId;
    }
}
