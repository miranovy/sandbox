<?php declare(strict_types=1);

namespace App\Modules\Base;

use App\Model\App;
use Nette\Application\AbortException;
use Nette\Security\IUserStorage;

abstract class SecuredPresenter extends BasePresenter
{

    /**
     * Check requirements.
     *
     * @param mixed $element
     * @throws AbortException
     */
    public function checkRequirements($element): void
    {
        if ($this->user->isLoggedIn() === false) {
            if ($this->user->getLogoutReason() === IUserStorage::INACTIVITY) {
                $this->flashInfo('You have been logged out for inactivity');
            }
          /*  $this->redirect(
                App::DESTINATION_SIGN_IN,
                ['backlink' => $this->storeRequest()]
            ); */
        }
    }
}
