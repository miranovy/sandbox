<?php

declare(strict_types=1);

namespace App\Modules\Base\Control;

use App\UI\Form\Rendering\AdminLteHorizontalRenderer;
use App\Modules\Admin\User\Form\UserFormFactory;
use App\UI\Control\BaseControl;
use App\UI\Form\BaseForm;

/**
 * Card form control.
 *
 * @author Miroslav Nový <miranovy@gmail.com>
 */
class CardFormControl extends BaseControl
{
    /** @var BaseForm Form */
    public $form;

    /** @var string Card title */
    private $title;

    /** @var string  Card header background color (e.g. primary, success, ...) */
    private $bgColor;

    /**
     * Class constructor.
     *
     * @param BaseForm $form Form
     * @param string $title Card title
     * @param string $bgColor Card header background color (e.g. primary, success, ...)
     */
    public function __construct(BaseForm $form, string $title, string $bgColor = 'primary')
    {
        $this->form = $form;
        $this->title = $title;
        $this->bgColor = $bgColor;
    }

    /**
     * Render contol.
     *
     * @return void
     */
    public function render()
    {
        $this->template->title = $this->title;
        $this->template->bgHeaderColor = $this->bgColor;
        $this->template->render(__DIR__ . '/templates/CardFormControl.latte');
    }

    /**
     * Return form component.
     *
     * @return BaseForm
     */
    public function createComponentForm(): BaseForm
    {
        return $this->form;
    }
}
