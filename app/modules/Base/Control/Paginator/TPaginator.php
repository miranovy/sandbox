<?php


namespace App\Modules\Base\Control\Paginator;

use Nette\Application\UI\Control;
use Nette\Utils\Paginator;

/**
 * Paginator control trait.
 */
trait TPaginator
{
    /** @var PaginatorControlFactory @inject */
    public $paginatorControlFactory;

    /** @var Paginator */
    private $paginator;

    /**
     * Create paginator.
     *
     * @param int $page Selected page number
     * @param int $itemCount Item count
     * @param int $itemsPerPage Items per page
     */
    public function createPaginator(int $page, int $itemCount, int $itemsPerPage): void
    {
        $this->paginator = new Paginator();
        $this->paginator->page = $page;
        $this->paginator->itemCount = $itemCount;
        $this->paginator->itemsPerPage = $itemsPerPage;
    }

    /**
     * Return paginator.
     *
     * @return Paginator
     */
    public function getPaginator(): Paginator
    {
        return $this->paginator;
    }

    /**
     * Create paginator control component.
     *
     * @return PaginatorControl
     */
    public function createComponentPaginator(): PaginatorControl
    {
        return $this->paginatorControlFactory->create($this->paginator);
    }
}
