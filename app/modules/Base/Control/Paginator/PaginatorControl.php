<?php declare(strict_types=1);


namespace App\Modules\Base\Control\Paginator;

use App\UI\Control\BaseControl;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\Utils\Paginator;

/**
 * Paginator control.
 */
class PaginatorControl extends BaseControl
{
    /** @var Paginator */
    private $paginator;

    /**
     * Class constructor.
     *
     * @param Paginator $paginator
     */
    public function __construct(Paginator $paginator)
    {
        $this->paginator = $paginator;
    }

    /**
     * Render control.
     *
     * @return void
     */
    public function render(): void
    {
        /** @var Template $template */
        $template = $this->template;

        $template->setFile(__DIR__ . '/template.latte');
        $template->paginator = $this->paginator;

        $template->render();
    }
}
