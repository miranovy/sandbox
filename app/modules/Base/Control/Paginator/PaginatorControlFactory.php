<?php declare(strict_types=1);


namespace App\Modules\Base\Control\Paginator;

use Nette\Utils\Paginator;

/**
 * Paginator control factory.
 */
class PaginatorControlFactory
{
    /**
     * Create paginator control.
     *
     * @param Paginator $paginator
     * @return PaginatorControl
     */
    public function create(Paginator $paginator): PaginatorControl
    {
        return new PaginatorControl($paginator);
    }
}
