<?php declare(strict_types=1);

namespace App\Modules\Admin;

use App\Model\App;
use App\Modules\Base\SecuredPresenter;
use Chap\AdminLTE\AdminControl;
use Chap\AdminLTE\IAdminControlFactory;
use Nette\Application\AbortException;

abstract class AdminPresenter extends SecuredPresenter
{
    /** @var IAdminControlFactory @inject */
    public $adminControlFactory;

    /**
     * Check requirements.
     *
     * @param mixed $element
     * @return void
     * @throws AbortException
     */
    public function checkRequirements($element): void
    {
        parent::checkRequirements($element);
        $this->checkUserAllowed('Admin:Home');
    }

    /**
     * Check if is user allowed.
     *
     * @param string $resource Resource
     * @param string $privilege Privilege
     * @param string $redirect Redirect url
     * @return void
     */
    protected function checkUserAllowed(string $resource, string $privilege = 'view', string $redirect = App::DESTINATION_ADMIN_HOMEPAGE): void
    {
        if ($this->user->isAllowed($resource, $privilege) === false) {
            $this->flashError('You cannot access this with user role');
        }
    }

    /**
     * Create admin control component.
     *
     * @return AdminControl
     */
    protected function createComponentAdmin(): AdminControl
    {
        return $this->adminControlFactory->create();
    }
}
