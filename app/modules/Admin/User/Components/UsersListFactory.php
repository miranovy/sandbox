<?php

declare(strict_types=1);

namespace App\Modules\Admin\User\Components;

use App\Model\Orm\Orm;
use App\Model\Orm\Users\User;
use App\UI\DataGrid\DataGrid;
use App\UI\DataGrid\IDataGridFactory;
use Nette\ComponentModel\IContainer;

/**
 * Users list component factory.
 */
class UsersListFactory
{
    /** @var IDataGridFactory Data grid factory */
    private $dataGridFactory;

    /** @var Orm Orm */
    private $orm;

    /**
     * Class constructor.
     *
     * @param IDataGridFactory $dataGridFactory
     * @param Orm $orm
     */
    public function __construct(IDataGridFactory $dataGridFactory, Orm $orm)
    {
        $this->dataGridFactory = $dataGridFactory;
        $this->orm = $orm;
    }

    public function create(?IContainer $parent, string $name): DataGrid
    {
        $grid = $this->dataGridFactory->create($parent, $name);

        //$grid->setPrimaryKey('userId');
        $grid->setDataSource($this->orm->users->findAll());

        // add columns
        $grid->addColumnText('name', 'user.name')
            ->setSortable()
            ->setSortableResetPagination()
            ->setFilterText();
        $grid->addColumnText('surname', 'user.surname')
            ->setSortable()
            ->setSortableResetPagination()
            ->setFilterText();
        $grid->addColumnText('email', 'user.email')
            ->setSortable()
            ->setSortableResetPagination()
            ->setFilterText();
        $grid->addColumnText('role', 'user.role')
            ->setReplacement($grid->translateColumnReplacement(User::ROLES))
            ->setSortable()
            ->setSortableResetPagination()
            ->setFilterSelect(['' => 'user.all'] + User::ROLES)
            ->setTranslateOptions();
        $grid->addColumnText('state', 'user.state')
            ->setReplacement($grid->translateColumnReplacement(User::STATES))
            ->setSortable()
            ->setSortableResetPagination()
            ->setFilterSelect(['' => 'user.all'] + User::STATES)
            ->setTranslateOptions();
        $grid->addColumnDateTime('lastLoggedAt', 'user.last_logged_at')
            ->setFormat('m.d.Y H:i:s')
            ->setSortable()
            ->setSortableResetPagination();

        $grid->addAction('detail', 'ublaboo_datagrid.title_detail', ':Admin:User:profile')
            ->setIcon('eye')
            ->setTitle('ublaboo_datagrid.detail');

        return $grid;
    }
}
