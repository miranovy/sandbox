<?php declare(strict_types=1);

namespace App\Modules\Admin\User\Form;

use App\Model\Orm\Orm;
use App\Model\Orm\Users\User;
use App\Model\Security\Passwords;
use App\UI\Form\BaseForm;
use App\UI\Form\FormFactory;
use App\UI\Form\OrmFormFactory;
use Nette\Forms\IControl;
use Nette\Localization\ITranslator;
use Nette\Utils\ArrayHash;
use Nextras\Orm\Entity\Entity;

/**
 * User form factory.
 */
class UserFormFactory extends OrmFormFactory
{

    /** @var Passwords */
    private $passwords;

    /**
     * Class constructor.
     *
     * @param ITranslator $translator
     * @param Orm $orm Orm
     * @param Passwords $passwords Passwords utility
     */
    public function __construct(ITranslator $translator, Orm $orm, Passwords $passwords)
    {
        parent::__construct($translator, $orm);
        $this->passwords = $passwords;
    }

    /**
     * Add items to form.
     *
     * @param BaseForm $form
     * @return BaseForm
     */
    public function addItems(BaseForm $form): BaseForm
    {
        $form->addText('name', 'user.form.name')
            ->setRequired('form.required_field')
            ->addRule(BaseForm::MIN_LENGTH, 'form.min_length', 2)
            ->addRule(BaseForm::MAX_LENGTH, 'form.max_length', 50);

        $form->addText('surname', 'user.form.surname')
            ->setRequired('form.required_field')
            ->addRule(BaseForm::MIN_LENGTH, 'form.min_length', 2)
            ->addRule(BaseForm::MAX_LENGTH, 'form.max_length', 50);

        $form->addText('email', 'user.form.email')
            ->setRequired('form.required_field')
            ->addRule(BaseForm::MAX_LENGTH, 'form.max_length', 50)
            ->addRule(BaseForm::EMAIL, 'form.invalid_email_value')
            ->addRule([$this, 'validateEmailDuplicity'], 'user.form.email_already_exists');

        $form->addPassword('password', 'user.form.password')
            ->setRequired('form.required_field')
            ->addRule(BaseForm::MIN_LENGTH, 'form.min_length', 8);

        $form->addSelect('role', 'user.form.role', User::ROLES)
            ->setRequired('form.required_field')
            ->setPrompt('user.form.select_role');

        $form->addSelect('state', 'user.form.state', User::STATES)
            ->setRequired('form.required_field')
            ->setPrompt('user.form.select_state');

        $form->addSubmit('submit', 'user.btn.create');

        return $form;
    }

    /**
     * @return array<string, mixed>
     */
    public function getDefaultValues(): array
    {
        return [
            'role' => User::ROLE_USER,
            'state' => User::STATE_FRESH,
        ];
    }

    /**
     * @param int $id
     * @return Entity|null
     */
    public function getEntity(int $id): ?Entity
    {
        return $this->orm->users->getById($id);
    }

    /**
     * @return Entity
     */
    public function createEntity(): Entity
    {
        return new User();
    }

    /**
     * @param Entity $entity
     * @param ArrayHash $values
     */
    public function updateEntity(Entity $entity, ArrayHash $values): void
    {
        /** @var User $user */
        $user = $entity;

        $user->name = $values->name;
        $user->surname = $values->surname;
        $user->email = $values->email;
        $user->role = $values->role;
        $user->state = $values->state;
        $user->password = $this->passwords->hash($values->password);
    }

    /**
     * Validate user email duplicity.
     *
     * @param IControl $input Control
     * @return bool
     */
    public function validateEmailDuplicity(IControl $input)
    {
        $user = $this->orm->users->getByEmail($input->getValue());

        return $user === null ? true : false;
    }
}
