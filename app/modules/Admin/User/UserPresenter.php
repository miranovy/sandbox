<?php

declare(strict_types=1);

namespace App\Modules\Admin\User;

use App\Model\Orm\Orm;
use App\Model\Orm\Users\User;
use App\Modules\Admin\AdminPresenter;
use App\Modules\Admin\User\Components\UsersListFactory;
use App\Modules\Admin\User\Form\UserFormFactory;
use App\Modules\Base\Control\CardFormControl;
use App\UI\Control\BaseControl;
use App\UI\Form\Rendering\AdminLteHorizontalRenderer;
use Ublaboo\DataGrid\DataGrid;

/**
 * User presenter.
 */
final class UserPresenter extends AdminPresenter
{
    /** @var UsersListFactory Users list factory @inject */
    public $usersListFactory;

    /** @var UserFormFactory User form factory @inject */
    public $userFormFactory;

    /** @var Orm Orm @inject */
    public $orm;

    /** @var int|null Edited user identifier */
    private $userId;

    /**
     * Check requirements.
     *
     * @param mixed $element
     * @return void
     */
    public function checkRequirements($element): void
    {
        parent::checkRequirements($element);
        $this->checkUserAllowed('Admin:User');
    }

    /**
     * Add new user action.
     *
     * @return void
     */
    public function actionAdd(): void
    {
        $this->checkUserAllowed('Admin:User', 'add');
    }

    /**
     * Show user profile action.
     *
     * @param int $id User identifier
     * @return void
     */
    public function actionProfile(int $id): void
    {
        $this->template->myUser = $this->getUserById($id);
        ;
    }

    /**
     * Create users list component
     *
     * @param string $name Grid name
     * @return DataGrid
     */
    public function createComponentUsersList(string $name): DataGrid
    {
        return $this->usersListFactory->create($this, $name);
    }

    /**
     * Create user form component.
     *
     * @return BaseControl
     */
    public function createComponentUserForm(): BaseControl
    {
        $form = $this->userFormFactory->create(null, function () {
            $this->redirect(':Admin:User:list');
        });
        $form->setRenderer(new AdminLteHorizontalRenderer());

        return new CardFormControl($form, 'user.title.add');
    }

    /**
     * Get user by user identifier.
     *
     * @param int $id User identifier
     * @return User
     */
    protected function getUserById(int $id): User
    {
        /** @var User|null $user */
        $user = $this->orm->users->getById($id);
        if ($user === null) {
            throw new \RuntimeException('User not found!');
        }

        return $user;
    }
}
