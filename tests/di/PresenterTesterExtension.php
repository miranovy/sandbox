<?php

declare(strict_types=1);

namespace Tests\DI;

use Mangoweb\Tester\HttpMocks\HttpRequest;
use Mangoweb\Tester\PresenterTester\PresenterTester;
use Nette;
use Nette\DI\CompilerExtension;
use Nette\DI\Statement;
use Nette\Http\Request;
use Nette\Http\UrlScript;
use Nette\Schema\Expect;
use Nette\Schema\Schema;

/**
 * Presenter tester DI extension.
 */
class PresenterTesterExtension extends CompilerExtension
{
    /**
     * Return configuration schema.
     *
     * @return Schema
     */
    public function getConfigSchema(): Schema
    {
        return Expect::structure([
            'baseUrl' => Expect::string('http://my-app.dev'),
        ]);
    }

    /**
     * Load configuration.
     */
    public function loadConfiguration(): void
    {
        $builder = $this->getContainerBuilder();
        /** @var Nette\Utils\ArrayHash $config */
        $config = $this->config;

        $builder->addDefinition($this->prefix('presenterTester'))
            ->setFactory(PresenterTester::class)
            ->setArgument('baseUrl', $config->baseUrl);
    }

    /**
     * Before compile.
     *
     * Change \Nette\Http\Request for \Mangoweb\Tester\HttpMocks\HttpRequest.
     */
    public function beforeCompile(): void
    {
        $builder = $this->getContainerBuilder();
        /** @var Nette\Utils\ArrayHash $config */
        $config = $this->config;

        if ($builder->hasDefinition('http.request')) {
            $builder->getDefinition('http.request')
                ->setClass(Request::class)
                ->setFactory(HttpRequest::class, [new Statement(UrlScript::class, [$config->baseUrl])]);
        }
    }
}
