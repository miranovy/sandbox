<?php declare(strict_types=1);

namespace Tests\Toolkit\TestCase;

use Mangoweb\Tester\PresenterTester\PresenterTester;
use Nette\DI\Container;

class PresenterTesterTestCase extends ContainerTestCase
{
    /** @var PresenterTester Presenter tester */
    protected $presenterTester;

    /**
     * Class constructor
     *
     * @param Container $container Nette DI container
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
        $this->presenterTester = $this->getServiceByType('Mangoweb\Tester\PresenterTester\PresenterTester');
    }
}
