<?php declare(strict_types=1);

namespace Tests\Toolkit\TestCase;

use Tester\TestCase;
use Faker;
use Faker\Generator;

/**
 * Base test case.
 */
abstract class BaseTestCase extends TestCase
{
    /** @var Generator Faker */
    protected $faker;

    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->faker = Faker\Factory::create();
    }
}
