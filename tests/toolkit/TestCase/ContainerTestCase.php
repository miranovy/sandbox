<?php declare(strict_types=1);

namespace Tests\Toolkit\TestCase;

use Nette\DI\Container;

/**
 * Container test case.
 */
abstract class ContainerTestCase extends BaseTestCase
{
    /** @var Container Nette DI container */
    protected $container;

    /**
     * Class constructor
     *
     * @param Container $container Nette DI container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        parent::__construct();
    }

    /**
     * Return service by type.
     *
     * @param string $type Service type
     * @return mixed|null
     */
    public function getServiceByType(string $type)
    {
        return $this->container->getByType($type);
    }

    /**
     * Return service by name.
     *
     * @param string $name Service name
     * @return mixed|null
     */
    public function getServiceByName(string $name)
    {
        return $this->container->getService($name);
    }

    /**
     * Return configuration parameter.
     *
     * @param string $key Parametr key
     * @return mixed|null
     */
    public function getParameter(string $key)
    {
        $params = $this->container->getParameters();

        // key with dots as key
        if (isset($params[$key])) {
            return $params[$key];
        }

        // key with dots as array separator
        $keys = explode('.', $key);
        while (count($keys) > 1) {
            $key = array_shift($keys);
            if (isset($params[$key]) === false) {
                return null;
            }
            $params = $params[$key];
        }

        $key = array_shift($keys);
        return $params[$key] ?? null;
    }
}
