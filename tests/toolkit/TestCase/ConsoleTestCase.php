<?php

declare(strict_types=1);

namespace Tests\Toolkit\TestCase;

use Nette\DI\Container;
use Contributte\Console\Application;

/**
 * Console test case.
 */
abstract class ConsoleTestCase extends ContainerTestCase
{

    /** @var Application|null Console application */
    protected $application;

    /**
     * Class constructor.
     *
     * @param Container $container Nette DI container
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
        $this->application = $this->getServiceByType('Contributte\Console\Application');
    }
}
