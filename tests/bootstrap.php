<?php

require __DIR__ . '/../vendor/autoload.php';

use Contributte\Bootstrap\ExtraConfigurator;

Tester\Environment::setup();

$configurator = new ExtraConfigurator();
$configurator->setTempDirectory(__DIR__ . '/../temp/tests');

// enable tracy logging
//$configurator->enableTracy(__DIR__ . '/../log');

// Provide some parameters
$configurator->addParameters([
    'rootDir' => realpath(__DIR__ . '/..'),
    'appDir' => realpath(__DIR__ . '/../app'),
    'wwwDir' => realpath(__DIR__ . '/../www'),
]);

// set configuration files
$configurator->addConfig(__DIR__ . '/../app/config/env/test.neon')
    ->addConfig(__DIR__ . '/../app/config/config.local.neon');

return $configurator->createContainer();
