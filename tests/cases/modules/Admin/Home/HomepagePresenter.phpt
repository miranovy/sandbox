<?php declare(strict_types=1);

namespace Tests\Modules\Admin\Home;

use App\Model\Orm\Users\User;
use App\Model\Security\Identity;
use App\Model\Utils\Strings;
use Tester\Assert;
use Tests\Toolkit\TestCase\PresenterTesterTestCase;

$container = require __DIR__ . '/../../../../bootstrap.php';

/**
 * Admin home presenter test.
 *
 * @testCase
 */
class HomepagePresenterTest extends PresenterTesterTestCase
{
    /**
     * Render dashboard for admin test.
     */
    public function testRenderDashboardForAdmin()
    {
        $request = $this->presenterTester->createRequest('Admin:Homepage')
            ->withIdentity(new Identity('admin', [User::ROLE_ADMIN]));

        $result = $this->presenterTester->execute($request);

        $source = $result->getTextResponseSource();
        $dom = @\Tester\DomQuery::fromHtml($source);
        $menuItems = $dom->find('li.nav-item a p');
        array_walk($menuItems, function (string &$item) {
            $item = Strings::trim($item);
        });
        Assert::true(in_array('Nástěnka', $menuItems), 'Exists admin menu item \'Nástěnka\'');
        Assert::true(in_array('Uživatelé', $menuItems), 'Exists admin menu item \'Uživatelé\'');
    }

    /**
     * Render dashboard for user test.
     *
     * public function testRenderDashboardForUser()
     * {
     * @fixme
     * $request = $this->presenterTester->createRequest('Admin:Homepage')
     * ->withIdentity(new Identity('admin', [User::ROLE_USER]));
     *
     * $result = $this->presenterTester->execute($request);
     *
     * $source = $result->getTextResponseSource();
     * $dom = @\Tester\DomQuery::fromHtml($source);
     * $menuItems = $dom->find('li.nav-item a p');
     * Assert::true(in_array('Nástěnka', $menuItems), 'Exists admin menu item \'Nástěnka\'');
     * Assert::false(in_array('Uživatelé', $menuItems), 'Exists admin menu item \'Uživatelé\'');
     * } */
}

$test = new HomepagePresenterTest($container);
$test->run();
